# 《Java开发企业级权限管理系统》 Spring Security Demo

## 课程地址

http://coding.imooc.com/class/149.html

## Apache Shiro Demo 项目

https://gitee.com/jiminzheng/shiro_demo

## 原生实现的权限管理项目（需要权限）

https://gitee.com/jiminzheng/permission

## 手记推荐

### 知识点索引
https://www.imooc.com/article/21443

### 课程问题汇总
https://www.imooc.com/article/21449

### 改造电商交易后台权限管理过程
https://www.imooc.com/article/20741

### 数据权限通用设计方案
https://www.imooc.com/article/21376

